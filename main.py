"""`main` is the top level module for your Flask application."""

# Import the Flask Framework
from flask import Flask, render_template,request

import pyimgur
import requests
import os
from base64 import b64encode

import imgur_credentials as cre

CLIENT_ID = cre.CLIENT_ID
client = pyimgur.Imgur(CLIENT_ID)

app = Flask(__name__)

def getTextFromImage(filelocation):
    try:
        import Image
    except ImportError: 
        from PIL import Image
    import pytesseract
    text = pytesseract.image_to_string(Image.open(filelocation))
    return text.lower()


@app.route('/', methods=['GET','POST'])
def index():
    if request.method == 'POST':
        data = b64encode(request.files['image'].read())
        r = client._send_request('https://api.imgur.com/3/image',method='POST',params={'image':data})
        resp = requests.get(r['link'])
        path = os.path.join(os.getenv('OPENSHIFT_DATA_DIR'),'yi.jpg')
        with open(path,'w') as f:
            f.write(resp.content)
        try:
            import Image
        except ImportError: 
            from PIL import Image
        import pytesseract
        text = pytesseract.image_to_string(Image.open('/'+path))
        return text
        #return render_template('transliterate.html',text=text)
    return render_template('index.html')


@app.errorhandler(404)
def page_not_found(e):
    """Return a custom 404 error when url is not found."""
    return 'Sorry, Nothing at this URL.', 404


@app.errorhandler(500)
def application_error(e):
    """Return a custom 500 error when there is server error."""
    return 'Sorry, unexpected error: {}'.format(e), 500

if __name__ == '__main__':
    app.run(debug=True)


